from typing import List, Type

from fastapi import Depends, HTTPException, status
from sqlalchemy.orm import Session
from fastapi import APIRouter
from pydantic import BaseModel

import src.core.config
from src.core.models import BaseCrud as bc


class BaseRouter:
    def __init__(
        self,
        router: APIRouter,
        base_schema: Type[BaseModel],
        creation_schema: Type[BaseModel],
        creation_check_fields: List[str],
        crud: Type[bc.BaseCrud]
    ):

        @router.post(
            "/",
            response_model=base_schema,
            status_code=status.HTTP_201_CREATED
        )
        def create(
            register_data: creation_schema,
            db: Session = Depends(src.core.config.get_db)
        ):
            for check_field in creation_check_fields:
                db_register = crud.get_by_field_name(
                    db, check_field, getattr(register_data, check_field)
                )
                if db_register:
                    raise HTTPException(status_code=400,
                                        detail=f"{check_field} already registered")
            return crud.create(db, register_data)

        @router.get(
            "/",
            response_model=List[base_schema]
        )
        def read_list(
            skip: int = 0,
            limit: int = 100,
            db: Session = Depends(src.core.config.get_db)
        ):
            db_list = crud.get_list(
                db, skip, limit
            )
            return db_list

        @router.get(
            "/{id}",
            response_model=base_schema
        )
        def read(register_id: int, db: Session = Depends(src.core.config.get_db)):
            db_register = crud.get_by_id(db, register_id)
            if db_register is None:
                raise HTTPException(status_code=404, detail="Not found")
            return db_register

        @router.delete(
            "/{id}",
            response_model=base_schema,
            status_code=status.HTTP_200_OK
        )
        def delete(register_id: int, db: Session = Depends(src.core.config.get_db)):
            db_register = crud.delete(db, register_id)
            if db_register is None:
                raise HTTPException(status_code=404, detail="Not found")
            return db_register

        @router.put(
            "/",
            response_model=base_schema,
            status_code=status.HTTP_200_OK
        )
        def put(schema: base_schema, db: Session = Depends(src.core.config.get_db)):
            db_register = crud.put(db, schema)
            if db_register is None:
                raise HTTPException(status_code=404, detail="Not found")
            return db_register

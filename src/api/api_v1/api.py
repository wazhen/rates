from fastapi import APIRouter


from .endpoints import (
    articles,
    commercialization_methods,
    article_ranges,
    countries,
    customer_categories,
    distribution_channels,
    customers
)

router = APIRouter()


router.include_router(
    articles.router,
    prefix="/articles",
    tags=["Articles"],
    responses={404: {"description": "Not found"}}
)

router.include_router(
    commercialization_methods.router,
    prefix="/commercialization_methods",
    tags=["Commercialization methods"],
    responses={404: {"description": "Not found"}}
)

router.include_router(
    article_ranges.router,
    prefix="/article_ranges",
    tags=["Article ranges"],
    responses={404: {"description": "Not found"}}
)

router.include_router(
    countries.router,
    prefix="/countries",
    tags=["Countries"],
    responses={404: {"description": "Not found"}}
)

router.include_router(
    customer_categories.router,
    prefix="/customer_categories",
    tags=["Customer categories"],
    responses={404: {"description": "Not found"}}
)

router.include_router(
    distribution_channels.router,
    prefix="/distribution_channels",
    tags=["Distribution channels"],
    responses={404: {"description": "Not found"}}
)

router.include_router(
    customers.router,
    prefix="/customers",
    tags=["Customers"],
    responses={404: {"description": "Not found"}}
)

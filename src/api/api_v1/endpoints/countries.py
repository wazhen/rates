from fastapi import APIRouter

from src.api import base_router as br
from src.core.models.countries import crud, schemas

router = APIRouter()

base_router = br.BaseRouter(
    router,
    base_schema=schemas.Country,
    creation_schema=schemas.CountryCreate,
    creation_check_fields=["name", "iso_2"],
    crud=crud.CountryCrud
)

from fastapi import APIRouter

from src.api import base_router as br
from src.core.models.article_ranges import crud, schemas

router = APIRouter()

base_router = br.BaseRouter(
    router,
    base_schema=schemas.ArticleRange,
    creation_schema=schemas.ArticleRangeCreate,
    creation_check_fields=["name"],
    crud=crud.ArticleRangeCrud
)

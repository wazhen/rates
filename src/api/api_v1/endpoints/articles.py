from fastapi import APIRouter

from src.api import base_router as br
from src.core.models.articles import crud, schemas

router = APIRouter()

base_router = br.BaseRouter(
    router,
    base_schema=schemas.Article,
    creation_schema=schemas.ArticleCreate,
    creation_check_fields=["sku", "name"],
    crud=crud.ArticlesCrud
)

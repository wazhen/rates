from fastapi import APIRouter

from src.api import base_router as br
from src.core.models.commercialization_methods import crud, schemas

router = APIRouter()

base_router = br.BaseRouter(
    router,
    base_schema=schemas.CommercializationMethod,
    creation_schema=schemas.CommercializationMethodCreate,
    creation_check_fields=["name"],
    crud=crud.CommercializationMethodCrud
)

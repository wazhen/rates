from fastapi import APIRouter

from src.api import base_router as br
from src.core.models.distribution_channels import crud, schemas

router = APIRouter()

base_router = br.BaseRouter(
    router,
    base_schema=schemas.DistributionChannel,
    creation_schema=schemas.DistributionChannelCreate,
    creation_check_fields=["name"],
    crud=crud.DistributionChannelCrud
)

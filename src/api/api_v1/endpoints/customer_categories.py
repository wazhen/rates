from fastapi import APIRouter

from src.api import base_router as br
from src.core.models.customer_categories import crud, schemas

router = APIRouter()

base_router = br.BaseRouter(
    router,
    base_schema=schemas.CustomerCategory,
    creation_schema=schemas.CustomerCategoryCreate,
    creation_check_fields=("name",),
    crud=crud.CustomerCategoryCrud
)

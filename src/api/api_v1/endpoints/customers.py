from fastapi import APIRouter

from src.api import base_router as br
from src.core.models.customers import crud, schemas

router = APIRouter()

base_router = br.BaseRouter(
    router,
    base_schema=schemas.Customer,
    creation_schema=schemas.CustomerCreate,
    creation_check_fields=["cif", "name"],
    crud=crud.CustomersCrud
)

import os

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from starlette.datastructures import CommaSeparatedStrings

ALLOWED_HOSTS = CommaSeparatedStrings(os.getenv("ALLOWED_HOSTS", ""))
API_V1_STR = "/api/v1"
PROJECT_NAME = "Tarifas"

db_engine = os.getenv("DATABASE_ENGINE")
db_user = os.getenv("DATABASE_USER")
db_password = os.getenv("DATABASE_PASSWORD")
db_name = os.getenv("DATABASE_NAME")
db_host = os.getenv("DATABASE_HOST")
db_port = os.getenv("DATABASE_PORT")

engine = create_engine(f"{db_engine}://{db_user}:{db_password}@{db_host}:{db_port}")
Session = sessionmaker(bind=engine, autocommit=False, autoflush=False)
session = Session()
Base = declarative_base()


def get_db():
    database = session
    try:
        yield database
    finally:
        database.close()

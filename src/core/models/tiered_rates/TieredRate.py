from sqlalchemy import Column, Integer, Float, Date, ForeignKey

import src.core.config


class TieredRate(src.core.config.Base):
    __tablename__ = "tiered_rates"

    id = Column(Integer, primary_key=True)
    customer_category_id = Column(Integer, ForeignKey("customer_categories.id"))
    distribution_channel_id = Column(Integer, ForeignKey("distribution_channels.id"))
    article_range_ic = Column(Integer, ForeignKey("article_ranges.id"))
    commercialization_method_id = Column(
        Integer, ForeignKey("commercialization_method.id")
    )
    country_id = Column(Integer, ForeignKey("countries.id"))
    review_date = Column(Date)
    discount_percentage_1 = Column(Float)
    threshold_1 = Column(Float)
    discount_percentage_2 = Column(Float)
    threshold_2 = Column(Float)
    discount_percentage_3 = Column(Float)

from typing import Optional
from pydantic import BaseModel


class CustomerCategoryBase(BaseModel):
    name: Optional[str]


class CustomerCategoryCreate(CustomerCategoryBase):
    name: str


class CustomerCategory(CustomerCategoryBase):
    id: int

    class Config:
        orm_mode = True

from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

import src.core.config


class CustomerCategory(src.core.config.Base):
    __tablename__ = "customer_categories"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    customers = relationship("Customer", backref="customer_category")
    rates = relationship("Rate", backref="customer_category")
    rate_actions = relationship("RateAction", backref="customer_category")
    tiered_rates = relationship("TieredRate", backref="customer_category")
    tiered_rate_actions = relationship("TieredRateAction", backref="customer_category")

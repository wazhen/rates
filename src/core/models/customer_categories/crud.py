from sqlalchemy.orm import Session

from src.core.models.customer_categories import CustomerCategory
from src.core.models.BaseCrud import BaseCrud


class CustomerCategoryCrud(BaseCrud):
    model = CustomerCategory.CustomerCategory
    create_columns = ("name",)

from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

import src.core.config


class ArticleRange(src.core.config.Base):
    __tablename__ = "article_ranges"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    articles = relationship("Article", backref="article_range")
    rates = relationship("Rate", backref="article_range")
    rate_actions = relationship("RateAction", backref="article_range")
    tiered_rates = relationship("TieredRate", backref="article_range")
    tiered_rate_actions = relationship("TieredRateAction", backref="article_range")

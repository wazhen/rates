from pydantic import BaseModel


class ArticleRangeBase(BaseModel):
    name: str


class ArticleRangeCreate(ArticleRangeBase):
    pass


class ArticleRange(ArticleRangeBase):
    id: int

    class Config:
        orm_mode = True

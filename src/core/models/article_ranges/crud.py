from sqlalchemy.orm import Session

from src.core.models.article_ranges import ArticleRange as ar
from src.core.models.BaseCrud import BaseCrud


class ArticleRangeCrud(BaseCrud):
    model = ar.ArticleRange
    create_columns = ("name",)

    @classmethod
    def get_by_name(cls, db: Session, ar_name: str):
        return db.query(ar.ArticleRange).filter(
            ar.ArticleRange.name == ar_name
        ).first()

import textwrap

from sqlalchemy import Column, Integer, Float, Date, ForeignKey, UniqueConstraint

import src.core.config


class Rate(src.core.config.Base):
    __tablename__ = "rates"

    id = Column(Integer, primary_key=True)
    customer_category_id = Column(
        Integer, ForeignKey("customer_categories.id"), nullable=False
    )
    distribution_channel_id = Column(
        Integer, ForeignKey("distribution_channels.id"), nullable=False
    )
    article_range_id = Column(Integer, ForeignKey("article_ranges.id"), nullable=False)
    commercialization_method_id = Column(
        Integer, ForeignKey("commercialization_method.id"), nullable=False
    )
    country_id = Column(Integer, ForeignKey("countries.id"), nullable=False)
    discount_percentage = Column(Float, nullable=False)
    review_date = Column(Date, nullable=False)

    __table_args__ = (
        UniqueConstraint(
            "customer_category_id",
            "distribution_channel_id",
            "article_range_id",
            "commercialization_method_id",
            "country_id",
        ),
    )

    def __eq__(self, other):
        if isinstance(other, Rate):
            return str(other.id) == str(self.id)
        else:
            return False

    def __str__(self):
        return textwrap.dedent(
            f"""
            id: {self.id}
            customer_category: {self.customer_category}
            distribution_channel: {self.distribution_channel}
            article_range: {self.article_range}
            commercialization_method: {self.commercialization_method}
            country: {self.country}
            discount_percentage: {self.discount_percentage}
            review_date: {self.review_date}
            """
        )

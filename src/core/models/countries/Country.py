from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

import src.core.config


class Country(src.core.config.Base):
    __tablename__ = "countries"

    id = Column(Integer, primary_key=True)
    name = Column(String, index=True, unique=True)
    iso_2 = Column(String, index=True, unique=True)
    rates = relationship("Rate", backref="country")
    rate_actions = relationship("RateAction", backref="country")
    tiered_rates = relationship("TieredRate", backref="country")
    tiered_rate_actions = relationship("TieredRateAction", backref="country")

    def __eq__(self, other):
        if not isinstance(other, Country):
            return False
        else:
            return str(other.id) == str(self.id)

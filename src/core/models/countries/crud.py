from sqlalchemy.orm import Session

from src.core.models.countries import Country as Ctr
from src.core.models.BaseCrud import BaseCrud


class CountryCrud(BaseCrud):
    model = Ctr.Country
    create_columns = ("name", "iso_2")

    @classmethod
    def get_by_name(cls, db: Session, name: str):
        return db.query(Ctr.Country).filter(Ctr.Country.name == name).first()

    @classmethod
    def get_by_iso_2(cls, db: Session, iso_2: str):
        return db.query(Ctr.Country).filter(Ctr.Country.iso_2 == iso_2).first()

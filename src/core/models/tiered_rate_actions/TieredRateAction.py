from sqlalchemy import Column, Integer, Float, Date, ForeignKey

import src.core.config


class TieredRateAction(src.core.config.Base):
    __tablename__ = "tiered_rate_actions"

    id = Column(Integer, primary_key=True)
    customer_category_id = Column(Integer, ForeignKey("customer_categories.id"))
    distribution_channel_id = Column(Integer, ForeignKey("distribution_channels.id"))
    article_range_id = Column(Integer, ForeignKey("article_ranges.id"))
    commercialization_method_id = Column(
        Integer, ForeignKey("commercialization_method.id")
    )
    country_id = Column(Integer, ForeignKey("countries.id"))
    customer_id = Column(Integer, ForeignKey("customers.id"))
    article_id = Column(Integer, ForeignKey("articles.id"))
    start_date = Column(Date)
    end_date = Column(Date)
    discount_percentage_1 = Column(Float)
    threshold_1 = Column(Float)
    discount_percentage_2 = Column(Float)
    threshold_2 = Column(Float)
    discount_percentage_3 = Column(Float)
    threshold_3 = Column(Float)

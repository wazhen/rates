from pydantic import BaseModel


class DistributionChannelBase(BaseModel):
    name: str


class DistributionChannelCreate(DistributionChannelBase):
    pass


class DistributionChannel(DistributionChannelBase):
    id: int

    class Config:
        orm_mode = True

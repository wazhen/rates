from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

import src.core.config


class DistributionChannel(src.core.config.Base):
    __tablename__ = "distribution_channels"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    customers = relationship("Customer", backref="distribution_channel")
    rates = relationship("Rate", backref="distribution_channel")
    rate_actions = relationship("RateAction", backref="distribution_channel")
    tiered_rates = relationship("TieredRate", backref="distribution_channel")
    tiered_rate_actions = relationship(
        "TieredRateAction", backref="distribution_channel"
    )

from sqlalchemy.orm import Session

from src.core.models.distribution_channels import DistributionChannel as cm
from src.core.models.BaseCrud import BaseCrud


class DistributionChannelCrud(BaseCrud):
    model = cm.DistributionChannel
    create_columns = ("name",)

    @classmethod
    def get_by_name(cls, db: Session, cm_name: str):
        return (
            db.query(cm.DistributionChannel)
            .filter(cm.DistributionChannel.name == cm_name)
            .first()
        )






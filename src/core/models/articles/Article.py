from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

import src.core.config


class Article(src.core.config.Base):
    __tablename__ = "articles"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    sku = Column(String, unique=True, nullable=False)
    commercialization_method_id = Column(
        Integer, ForeignKey("commercialization_method.id"), nullable=False
    )
    article_range_id = Column(Integer, ForeignKey("article_ranges.id"), nullable=False)
    rate_actions = relationship("RateAction", backref="article")
    tiered_rate_actions = relationship("TieredRateAction", backref="article")

    def __eq__(self, other):
        if not isinstance(other, Article):
            return False
        else:
            return str(other.id) == str(self.id)

from sqlalchemy.orm import Session

from src.core.models.articles import Article
from src.core.models.BaseCrud import BaseCrud


class ArticlesCrud(BaseCrud):
    model = Article.Article
    create_columns = ("name", "sku", "commercialization_method_id", "article_range_id")

    @classmethod
    def get_by_sku(cls, db: Session, article_sku: str):
        return db.query(Article.Article).filter(
            Article.Article.sku == article_sku).first()

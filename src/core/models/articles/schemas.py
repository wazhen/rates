from typing import Optional
from pydantic import BaseModel


class ArticleBase(BaseModel):
    name: str
    sku: str
    commercialization_method_id: Optional[int]
    article_range_id: Optional[int]


class ArticleCreate(ArticleBase):
    commercialization_method_id = int
    article_range_id = int


class Article(ArticleBase):
    id: int

    class Config:
        orm_mode = True

import textwrap

from sqlalchemy import Column, Integer, Float, Date, ForeignKey
from sqlalchemy.event import listens_for
from sqlalchemy.orm import validates

import src.core.config
from src.core.models.rate_actions.exceptions.RateActionArticleRelatedError import RateActionArticleRelatedError
from src.core.models.rate_actions.exceptions.RateActionCustomerRelatedError import RateActionCustomerRelatedError
from src.core.models.rate_actions.exceptions.RateActionNoRelationError import RateActionNoRelationError


class RateAction(src.core.config.Base):
    __tablename__ = "rate_actions"
    id = Column(Integer, primary_key=True)
    customer_category_id = Column(Integer, ForeignKey("customer_categories.id"))
    distribution_channel_id = Column(Integer, ForeignKey("distribution_channels.id"))
    article_range_id = Column(Integer, ForeignKey("article_ranges.id"))
    commercialization_method_id = Column(
        Integer, ForeignKey("commercialization_method.id")
    )
    country_id = Column(Integer, ForeignKey("countries.id"))
    customer_id = Column(Integer, ForeignKey("customers.id"))
    article_id = Column(Integer, ForeignKey("articles.id"))
    discount_percentage = Column(Float, nullable=False)
    start_date = Column(Date)
    end_date = Column(Date)

    def __eq__(self, other):
        if not isinstance(other, RateAction):
            return False
        else:
            return str(other.id) == str(self.id)

    @validates("customer")
    def validate_customer_id(self, key, value):
        if value:
            if self.customer_category or self.distribution_channel:
                raise ValueError()
        return value

    @validates("article")
    def validate_article_id(self, key, value):
        if value:
            if self.article_range or self.commercialization_method:
                raise ValueError()
        return value

    def __str__(self):
        return textwrap.dedent(
            f"""
            id: {self.id}
            distribution_channel: {self.distribution_channel}
            article_range: {self.article_range}
            commercialization_method: {self.commercialization_method}
            country: {self.country}
            customer: {self.customer}
            article: {self.article}
            discount_percentage: {self.discount_percentage}
            start_date: {self.start_date}
            end_date: {self.end_date}
            """
        )


class RateActionEvents:
    @staticmethod
    @listens_for(RateAction, "before_insert")
    @listens_for(RateAction, "before_update")
    def before_insert_update(mapper, connect, target):
        RateActionEvents.check_not_all_parents_empty(target)
        RateActionEvents.check_article_constraints(target)
        RateActionEvents.check_customer_constraints(target)

    @staticmethod
    def check_not_all_parents_empty(target):
        required_one_not_none = [
            target.customer_category,
            target.distribution_channel,
            target.article_range,
            target.commercialization_method,
            target.country,
            target.customer,
            target.article,
        ]
        if sum([bool(item) for item in required_one_not_none]) <= 0:
            raise RateActionNoRelationError()

    @staticmethod
    def check_customer_constraints(target):
        if target.customer_category:
            if not target.distribution_channel:
                raise RateActionCustomerRelatedError()

        if target.distribution_channel:
            if not target.customer_category:
                raise RateActionCustomerRelatedError()

    @staticmethod
    def check_article_constraints(target):
        if target.article_range:
            if not target.commercialization_method:
                raise RateActionArticleRelatedError()

        if target.commercialization_method:
            if not target.article_range:
                raise RateActionArticleRelatedError()

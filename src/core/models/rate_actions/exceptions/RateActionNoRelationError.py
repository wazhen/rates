import textwrap


class RateActionNoRelationError(Exception):
    def __init__(self):
        super(RateActionNoRelationError, self).__init__(
            textwrap.dedent(
                """
            RateAction must have at least one of the following relations:
            - customer_category,
            - distribution_channel,
            - article_range,
            - commercialization_method,
            - country,
            - customer,
            - article,
            """
            )
        )

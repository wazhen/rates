import textwrap


class RateActionCustomerRelatedError(Exception):
    def __init__(self):
        super(RateActionCustomerRelatedError, self).__init__(
            textwrap.dedent(
                """
            Customer related fields are:
            - Customer category
            - Distribution channel
            - Customer
            
            They must meet one of these two conditions:
            - Only Customer (other two empty)
            - Customer category and Distribution channel (Customer empty)
            """
            )
        )

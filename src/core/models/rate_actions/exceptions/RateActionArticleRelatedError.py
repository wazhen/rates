import textwrap


class RateActionArticleRelatedError(Exception):
    def __init__(self):
        super(RateActionArticleRelatedError, self).__init__(
            textwrap.dedent(
                """
                Article related fields are:
                - Article range
                - Commercialization method
                - Article
                
                They must meet one of these two conditions:
                - Only article (other two empty)
                - Article range and commercialization method (Article empty)
            """
            )
        )

from pydantic import BaseModel


class CommercializationMethodBase(BaseModel):
    name: str


class CommercializationMethodCreate(CommercializationMethodBase):
    pass


class CommercializationMethod(CommercializationMethodBase):
    id: int

    class Config:
        orm_mode = True

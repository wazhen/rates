from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

import src.core.config


class CommercializationMethod(src.core.config.Base):
    __tablename__ = "commercialization_method"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True)
    articles = relationship("Article", backref="commercialization_method")
    rates = relationship("Rate", backref="commercialization_method")
    rate_actions = relationship("RateAction", backref="commercialization_method")
    tiered_rates = relationship("TieredRate", backref="commercialization_method")
    tiered_rate_actions = relationship(
        "TieredRateAction", backref="commercialization_method"
    )

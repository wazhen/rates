from sqlalchemy.orm import Session

from src.core.models.commercialization_methods import CommercializationMethod as cm
from src.core.models.BaseCrud import BaseCrud


class CommercializationMethodCrud(BaseCrud):
    model = cm.CommercializationMethod
    create_columns = ("name",)

    @classmethod
    def get_by_name(cls, db: Session, cm_name: str):
        return (
            db.query(cm.CommercializationMethod)
            .filter(cm.CommercializationMethod.name == cm_name)
            .first()
        )






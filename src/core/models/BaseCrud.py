from sqlalchemy.orm import Session


class BaseCrud:
    model = None
    create_columns = []

    def __new__(cls, *args, **kwargs):
        if cls is BaseCrud:
            raise TypeError(
                "BaseCrud may not be instantiated. Abstract class for being inherited"
            )
        return object.__new__(cls, *args, **kwargs)

    @classmethod
    def get_by_id(cls, db: Session, register_id: int):
        return db.query(cls.model).filter(
            cls.model.id == register_id).first()

    @classmethod
    def get_by_field_name(cls, db: Session, field_name: str, field_value):
        return db.query(cls.model).filter(
            getattr(cls.model, field_name) == field_value
        ).first()

    @classmethod
    def get_list(cls, db: Session, skip: int = 0, limit: int = 100):
        return db.query(cls.model).offset(skip).limit(limit).all()

    @classmethod
    def create(cls, db: Session, data):
        db_register = cls.model(
            **{k: v for (k, v) in data.__dict__.items() if k in cls.create_columns}
        )
        db.add(db_register)
        db.commit()
        db.refresh(db_register)
        return db_register

    @classmethod
    def put(cls, db: Session, data):
        db_register = cls.get_by_id(db, data.id)
        for (k, v) in data.__dict__.items:
            db_register.k = v
        db.commit()
        db.refresh(db_register)
        return db_register

    @classmethod
    def delete(cls, db: Session, id: int):
        db_register = cls.get_by_id(db, id)
        db.delete(db_register)
        db.commit()
        return db_register

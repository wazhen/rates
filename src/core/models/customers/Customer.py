from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

import src.core.config


class Customer(src.core.config.Base):
    __tablename__ = "customers"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    cif = Column(String, unique=True)
    customer_category_id = Column(Integer, ForeignKey("customer_categories.id"))
    distribution_channel_id = Column(Integer, ForeignKey("distribution_channels.id"))
    rate_actions = relationship("RateAction", backref="customer")
    tiered_rate_actions = relationship("TieredRateAction", backref="customer")

    def __eq__(self, other):
        if not isinstance(other, Customer):
            return False
        else:
            return str(other.id) == str(self.id)

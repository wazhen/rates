from sqlalchemy.orm import Session

from src.core.models.customers import Customer
from src.core.models.BaseCrud import BaseCrud


class CustomersCrud(BaseCrud):
    model = Customer.Customer
    create_columns = ("name", "cif", "customer_category_id", "distribution_channel_id")

    @classmethod
    def get_by_cif(cls, db: Session, customer_cif: str):
        return db.query(Customer.Customer).filter(
            Customer.Customer.cif == customer_cif).first()

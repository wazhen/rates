from typing import Optional
from pydantic import BaseModel


class CustomerBase(BaseModel):
    name: str
    cif: str
    customer_category_id: Optional[int]
    distribution_channel_id: Optional[int]


class CustomerCreate(CustomerBase):
    customer_category_id = int
    distribution_channel_id = int


class Customer(CustomerBase):
    id: int

    class Config:
        orm_mode = True

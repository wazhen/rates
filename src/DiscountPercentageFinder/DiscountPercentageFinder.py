from .RateActionFinder.RateActionFinder import RateActionFinder
from .RateFinder.RateFinder import RateFinder
from .RateInterface.RateInterface import RateInterface


class DiscountPercentageFinder(RateInterface):
    def get_rate(self):
        match_rates = []
        rate_finder = RateFinder(
            session=self.session,
            customer_category=self.customer_category,
            distribution_channel=self.distribution_channel,
            article_range=self.article_range,
            commercialization_method=self.commercialization_method,
            country_iso_2=self.country_iso_2,
        )
        match_rates.append(rate_finder.get_rate())

        rate_action_finder = RateActionFinder(
            session=self.session,
            customer_category=self.customer_category,
            distribution_channel=self.distribution_channel,
            article_range=self.article_range,
            commercialization_method=self.commercialization_method,
            country_iso_2=self.country_iso_2,
            customer_cif=self.customer_cif,
            article_sku=self.article_sku,
        )
        match_rates.extend(rate_action_finder.search_all_combinations())
        return sorted(match_rates, key=lambda obj: obj.discount_percentage)[0]

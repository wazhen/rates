from sqlalchemy.orm import Session

from src.core.models.article_ranges.ArticleRange import ArticleRange
from src.core.models.commercialization_methods.CommercializationMethod import (
    CommercializationMethod,
)
from src.core.models.customer_categories.CustomerCategory import CustomerCategory
from src.core.models.distribution_channels.DistributionChannel import DistributionChannel


class RateInterface:
    def __init__(
        self,
        session: Session,
        customer_category=None,
        distribution_channel=None,
        article_range=None,
        commercialization_method=None,
        country_iso_2=None,
        customer_cif=None,
        article_sku=None,
    ):
        self.session = session
        self.customer_category = customer_category
        self.distribution_channel = distribution_channel
        self.article_range = article_range
        self.commercialization_method = commercialization_method
        self.country_iso_2 = country_iso_2
        self.customer_cif = customer_cif
        self.article_sku = article_sku
        self._validate_params()

    def _validate_params(self):
        assert isinstance(self.session, Session)
        self._validate_types()
        self._validate_customer()
        self._validate_article()
        self._validate_at_least_one()

    def _validate_article(self):
        if self.article_sku:
            assert not self.article_range
            assert not self.commercialization_method
        if self.article_range:
            assert not self.article_sku
            assert self.commercialization_method
        if self.commercialization_method:
            assert not self.article_sku
            assert self.article_range

    def _validate_customer(self):
        if self.customer_cif:
            assert not self.customer_category
            assert not self.distribution_channel

        if self.customer_category:
            assert self.distribution_channel
            assert not self.customer_cif

        if self.distribution_channel:
            assert self.customer_category
            assert not self.customer_cif

    def _validate_at_least_one(self):
        at_least_one = [
            self.customer_category,
            self.distribution_channel,
            self.article_range,
            self.commercialization_method,
            self.country_iso_2,
            self.customer_cif,
            self.article_sku,
        ]
        if sum([bool(item) for item in at_least_one]) == 0:
            raise ValueError("At least one parameter needed")

    def _validate_types(self):
        if self.customer_category and not isinstance(
            self.customer_category, CustomerCategory
        ):
            raise ValueError(
                "customer_category should be an instance of CustomerCategory"
            )

        if self.distribution_channel and not isinstance(
            self.distribution_channel, DistributionChannel
        ):
            raise ValueError(
                "distribution_channel should be an instance of DistributionChannel"
            )

        if self.article_range and not isinstance(self.article_range, ArticleRange):
            raise ValueError("article_range should be an instance if ArticleRange")

        if self.commercialization_method and not isinstance(
            self.commercialization_method, CommercializationMethod
        ):
            raise ValueError(
                "commercialization_method should be an instance of CommercializationMethod"
            )

    @staticmethod
    def add_parent_to_query(query, parent_list):
        for parent in parent_list:
            if parent:
                query = query.with_parent(parent)
        return query

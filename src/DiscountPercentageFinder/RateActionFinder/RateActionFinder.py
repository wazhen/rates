from src.core.models.articles.Article import Article
from src.core.models.countries.Country import Country
from src.core.models.customers.Customer import Customer
from src.core.models.rate_actions.RateAction import RateAction
from ..RateInterface.RateInterface import RateInterface


class RateActionFinder(RateInterface):
    def __init__(self, *args, **kwargs):
        super(RateActionFinder, self).__init__(*args, **kwargs)
        self.rate_action_list = []
        self.country = self.get_country(self.country_iso_2)
        self.customer = self.get_customer(self.customer_cif)
        self.article = self.get_article(self.article_sku)

    def search_all_combinations(self):
        results = []
        results.extend(self.get_a2_c2_country_rate_list())
        results.extend(self.get_article_customer_rate_list())
        results.extend(self.get_customer_country_rate_list())
        results.extend(self.get_article_country_rate_list())
        results.extend(self.get_a2_customer_rate_list())
        results.extend(self.get_article_c2_rate_list())
        results.extend(self.get_article_customer_country_rate_list())
        results.extend(self.get_a2_customer_country_rate_list())
        results.extend(self.get_article_c2_country_rate_list())
        results.extend(self.get_customer_rate_rate_list())
        results.extend(self.get_article_rate_rate_list())
        results.extend(self.get_c2_rate_list())
        results.extend(self.get_c2_country_rate_list())
        results.extend(self.get_a2_rate_list())
        results.extend(self.get_a2_country_rate_list())
        results.extend(self.get_a2_c2_rate_list())
        return results

    def get_article(self, article_sku):
        article = (
            self.session.query(Article).filter(Article.sku == article_sku).one()
            if article_sku
            else None
        )
        return article

    def get_customer(self, customer_cif):
        customer = (
            self.session.query(Customer).filter(Customer.cif == customer_cif).one()
            if customer_cif
            else None
        )
        return customer

    def get_country(self, country_iso_2):
        return self.session.query(Country).filter(Country.iso_2 == country_iso_2).one()

    def get_a2_list(self):
        return [self.article_range, self.commercialization_method]

    def get_c2_list(self):
        return [self.customer_category, self.distribution_channel]

    def get_article_customer_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction), [self.article, self.customer,]
        )
        return rate_action_query.all()

    def get_customer_country_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction), [self.customer, self.country,]
        )
        return rate_action_query.all()

    def get_article_country_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction), [self.article, self.country,]
        )
        return rate_action_query.all()

    def get_a2_customer_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction), [*self.get_a2_list(), self.customer]
        )
        return rate_action_query.all()

    def get_article_c2_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction), [self.article, *self.get_c2_list(),]
        )
        return rate_action_query.all()

    def get_article_customer_country_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction), [self.article, self.customer, self.country,]
        )
        return rate_action_query.all()

    def get_a2_customer_country_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction),
            [*self.get_a2_list(), self.customer, self.country,],
        )
        return rate_action_query.all()

    def get_article_c2_country_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction),
            [self.article, *self.get_c2_list(), self.country],
        )
        return rate_action_query.all()

    def get_customer_rate_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction), [self.customer,]
        )
        return rate_action_query.all()

    def get_article_rate_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction), [self.article,]
        )
        return rate_action_query.all()

    def get_c2_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction), [*self.get_c2_list(),]
        )
        return rate_action_query.all()

    def get_c2_country_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction), [*self.get_c2_list(), self.country]
        )
        return rate_action_query.all()

    def get_a2_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction), [*self.get_a2_list(),]
        )
        return rate_action_query.all()

    def get_a2_country_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction), [*self.get_a2_list(), self.country]
        )
        return rate_action_query.all()

    def get_a2_c2_country_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction),
            [*self.get_a2_list(), *self.get_c2_list(), self.country],
        )
        return rate_action_query.all()

    def get_a2_c2_rate_list(self):
        rate_action_query = RateActionFinder.add_parent_to_query(
            self.session.query(RateAction), [*self.get_a2_list(), *self.get_c2_list(),]
        )
        return rate_action_query.all()

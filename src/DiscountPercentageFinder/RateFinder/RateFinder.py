from src.core.models.articles.Article import Article
from src.core.models.countries.Country import Country
from src.core.models.customers.Customer import Customer
from src.core.models.rates.Rate import Rate
from ..RateInterface.RateInterface import RateInterface


class RateFinder(RateInterface):
    def __init__(self, *args, **kwargs):
        super(RateFinder, self).__init__(*args, **kwargs)
        self.country = self.get_country()
        self.customer = self.get_customer()
        self.article = self.get_article()
        self._validate_mandatory_parameters()

    def get_country(self):
        if self.country_iso_2:
            country = (
                self.session.query(Country)
                .filter(Country.iso_2 == self.country_iso_2)
                .one()
            )
            return country

    def get_customer(self):
        if self.customer_cif:
            customer = (
                self.session.query(Customer)
                .filter(Customer.cif == self.customer_cif)
                .one()
            )
            self.customer_category = customer.customer_category
            self.distribution_channel = customer.distribution_channel
            return customer

    def get_article(self):
        if self.article_sku:
            article = (
                self.session.query(Article)
                .filter(Article.sku == self.article_sku)
                .one()
            )
            self.article_range = article.article_range
            self.commercialization_method = article.commercialization_method
            return article

    def get_rate(self):
        rate_query = RateFinder.add_parent_to_query(
            self.session.query(Rate),
            [
                self.country,
                self.customer_category,
                self.distribution_channel,
                self.article_range,
                self.commercialization_method,
            ],
        )
        return rate_query.one()

    def _validate_mandatory_parameters(self):
        self._validate_country()

    def _validate_country(self):
        assert bool(self.country_iso_2)

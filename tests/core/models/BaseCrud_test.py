import pytest

from src.core.models import BaseCrud
from src.core.models.articles.crud import ArticlesCrud


class TestBaseCrud:
    def test_instantiate_base_crud(self):
        with pytest.raises(TypeError):
            BaseCrud.BaseCrud()

    def test_subclass_instantiation_allowed(self):
        ArticlesCrud()

from src.core.models.distribution_channels.DistributionChannel import DistributionChannel
from .factories import DistributionChannelFactory


def test_create_distribution_channel(session):
    quantity = 5
    DistributionChannelFactory.create_batch(quantity)
    assert session.query(DistributionChannel).count() == quantity

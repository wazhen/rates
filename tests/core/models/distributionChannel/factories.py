import factory

from src.core.models.distribution_channels.DistributionChannel import DistributionChannel
from tests import config


class DistributionChannelFactory(factory.alchemy.SQLAlchemyModelFactory):
    id = factory.Sequence(lambda n: "%s" % n)
    name = factory.Faker("word")

    class Meta:
        model = DistributionChannel
        sqlalchemy_session = config.Session
        sqlalchemy_get_or_create = ("name",)

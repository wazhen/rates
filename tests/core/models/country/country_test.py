from src.core.models.countries.Country import Country
from .factories import CountryFactory


class TestCountry:
    class TestDunders:
        def test_eq(self, session):
            country = CountryFactory.create()
            session.query(Country).all()
            assert not country == 1
            assert not country == "Abc"
            assert not country == [1, "Abc"]

    def test_create_country(self, session):
        quantity = 5
        CountryFactory.create_batch(quantity)
        assert session.query(Country).count() == quantity

    def test_2_countries_same_name_iso_2(self, session):
        CountryFactory.create(iso_2="ES")
        CountryFactory.create(iso_2="ES")
        assert session.query(Country).count() == 1

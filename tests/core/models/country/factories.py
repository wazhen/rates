import factory
import pycountry

from src.core.models.countries.Country import Country
from tests import config


class CountryFactory(factory.alchemy.SQLAlchemyModelFactory):
    id = factory.Sequence(lambda n: "%s" % n)
    iso_2 = factory.Faker("country_code")
    name = factory.LazyAttribute(
        lambda o: pycountry.countries.get(alpha_2=o.iso_2).name
    )

    class Meta:
        model = Country
        sqlalchemy_session = config.Session
        sqlalchemy_get_or_create = ("iso_2", "name")

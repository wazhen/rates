import pytest

from src.core.models.rate_actions.exceptions.RateActionArticleRelatedError import (
    RateActionArticleRelatedError,
)
from src.core.models.rate_actions.exceptions.RateActionCustomerRelatedError import (
    RateActionCustomerRelatedError,
)
from src.core.models.rate_actions.exceptions.RateActionNoRelationError import (
    RateActionNoRelationError,
)
from .factories import (
    RateActionBaseFactory,
    RateActionFactory,
    RateActionNoParentsFactory,
    RateAction,
)


@pytest.mark.rate_action
class TestCreationExpectErrors:
    class TestDunders:
        def test_eq(self, session):
            rate_action = RateActionFactory.Valid.Mixed.OnlyCountry.create()
            session.query(RateAction).all()
            assert not rate_action == 1
            assert not rate_action == "Abc"
            assert not rate_action == [1, "Abc"]

    def test_all_fields(self, session):
        with pytest.raises(ValueError):
            RateActionBaseFactory.create()
            session.query(RateAction).all()

    def test_customer_customer_category_distribution_channel(self, session):
        with pytest.raises(ValueError):
            RateActionFactory.Invalid.CustomerRelated.CustomerCustomerCategoryDistributionChannel.create()
            session.query(RateAction).all()

    def test_article_article_range_commercialization_method(self, session):
        with pytest.raises(ValueError):
            RateActionFactory.Invalid.ArticleRelated.ArticleArticleRateCommercializationMethod.create()
            session.query(RateAction).all()

    def test_no_parents(self, session):
        with pytest.raises(RateActionNoRelationError):
            RateActionNoParentsFactory.create()
            session.query(RateAction).all()

    def test_only_customer_category(self, session):
        with pytest.raises(RateActionCustomerRelatedError):
            RateActionFactory.Invalid.CustomerRelated.CustomerCategory.create()
            session.query(RateAction).all()

    def test_only_distribution_channel(self, session):
        with pytest.raises(RateActionCustomerRelatedError):
            RateActionFactory.Invalid.CustomerRelated.DistributionChannel.create()
            session.query(RateAction).all()

    def test_only_article_range(self, session):
        with pytest.raises(RateActionArticleRelatedError):
            RateActionFactory.Invalid.ArticleRelated.ArticleRange.create()
            session.query(RateAction).all()

    def test_only_commercialization_method(self, session):
        with pytest.raises(RateActionArticleRelatedError):
            RateActionFactory.Invalid.ArticleRelated.CommercializationMethod.create()
            session.query(RateAction).all()


@pytest.mark.rate_action
class TestCreationExpectSuccess:
    def test_only_country(self, session):
        quantity = 5
        RateActionFactory.Valid.Mixed.OnlyCountry.create_batch(quantity)
        assert session.query(RateAction).count() == quantity

    def test_only_article(self, session):
        quantity = 5
        RateActionFactory.Valid.ArticleRelated.Article.create_batch(quantity)
        assert session.query(RateAction).count() == quantity

    def test_a2(self, session):
        quantity = 5
        RateActionFactory.Valid.ArticleRelated.A2.create_batch(quantity)
        assert session.query(RateAction).count() == quantity

    def test_only_customer(self, session):
        quantity = 5
        RateActionFactory.Valid.CustomerRelated.Customer.create_batch(quantity)
        assert session.query(RateAction).count() == quantity

    def test_c2(self, session):
        quantity = 5
        RateActionFactory.Valid.CustomerRelated.C2.create_batch(quantity)
        assert session.query(RateAction).count() == quantity


@pytest.mark.rate_action
class TestRateActionMethods:
    def test_parse_string(self, session):
        rate_action = RateActionFactory.Valid.ArticleRelated.Article.create()
        assert str(rate_action)

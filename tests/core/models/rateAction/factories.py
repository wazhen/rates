import random
from datetime import timedelta

import factory

from src.core.models.rate_actions.RateAction import RateAction
from ..article.factories import ArticleFactory
from ..articleRange.factories import ArticleRangeFactory
from ..commercializationMethod.factories import CommercializationMethodFactory
from ..country.factories import CountryFactory
from ..customer.factories import CustomerFactory
from ..customerCategory.factories import CustomerCategoryFactory
from ..distributionChannel.factories import DistributionChannelFactory
from tests import config


class RateActionBaseFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = RateAction
        sqlalchemy_session = config.Session

    id = factory.Sequence(lambda n: "%s" % n)
    customer_category = factory.SubFactory(CustomerCategoryFactory)
    distribution_channel = factory.SubFactory(DistributionChannelFactory)
    article_range = factory.SubFactory(ArticleRangeFactory)
    commercialization_method = factory.SubFactory(CommercializationMethodFactory)
    country = factory.SubFactory(CountryFactory)
    customer = factory.SubFactory(CustomerFactory)
    article = factory.SubFactory(ArticleFactory)
    discount_percentage = factory.lazy_attribute(lambda obj: random.random())
    start_date = factory.Faker("date_object")
    end_date = factory.lazy_attribute(
        lambda obj: obj.start_date + timedelta(days=random.randint(1, 365))
    )


class RateActionNoParentsFactory(RateActionBaseFactory):
    customer_category = None
    distribution_channel = None
    article_range = None
    commercialization_method = None
    country = None
    customer = None
    article = None


class RateActionFactory:
    class Invalid:
        class CustomerRelated:
            class CustomerCustomerCategoryDistributionChannel(
                RateActionNoParentsFactory
            ):
                customer_category = factory.SubFactory(CustomerCategoryFactory)
                distribution_channel = factory.SubFactory(DistributionChannelFactory)
                customer = factory.SubFactory(CustomerFactory)

            class CustomerCategory(RateActionNoParentsFactory):
                customer_category = factory.SubFactory(CustomerCategoryFactory)

            class DistributionChannel(RateActionNoParentsFactory):
                distribution_channel = factory.SubFactory(DistributionChannelFactory)

        class ArticleRelated:
            class ArticleArticleRateCommercializationMethod(RateActionNoParentsFactory):
                article_range = factory.SubFactory(ArticleRangeFactory)
                commercialization_method = factory.SubFactory(
                    CommercializationMethodFactory
                )
                article = factory.SubFactory(ArticleFactory)

            class ArticleRange(RateActionNoParentsFactory):
                article_range = factory.SubFactory(ArticleRangeFactory)

            class CommercializationMethod(RateActionNoParentsFactory):
                commercialization_method = factory.SubFactory(
                    CommercializationMethodFactory
                )

    class Valid:
        class ArticleRelated:
            class Article(RateActionNoParentsFactory):
                article = factory.SubFactory(ArticleFactory)

            class A2(RateActionNoParentsFactory):
                article_range = factory.SubFactory(ArticleRangeFactory)
                commercialization_method = factory.SubFactory(
                    CommercializationMethodFactory
                )

        class CustomerRelated:
            class Customer(RateActionNoParentsFactory):
                customer = factory.SubFactory(CustomerFactory)

            class C2(RateActionNoParentsFactory):
                customer_category = factory.SubFactory(CustomerCategoryFactory)
                distribution_channel = factory.SubFactory(DistributionChannelFactory)

        class Mixed:
            class A2C2Country(RateActionBaseFactory):
                customer = None
                article = None

            class ArticleCustomer(RateActionNoParentsFactory):
                customer = factory.SubFactory(CustomerFactory)
                article = factory.SubFactory(ArticleFactory)

            class CustomerCountry(RateActionNoParentsFactory):
                customer = factory.SubFactory(CustomerFactory)
                country = factory.SubFactory(CountryFactory)

            class ArticleCountry(RateActionNoParentsFactory):
                article = factory.SubFactory(ArticleFactory)
                country = factory.SubFactory(CountryFactory)

            class A2Customer(RateActionNoParentsFactory):
                article_range = factory.SubFactory(ArticleRangeFactory)
                commercialization_method = factory.SubFactory(
                    CommercializationMethodFactory
                )
                customer = factory.SubFactory(CustomerFactory)

            class ArticleC2(RateActionNoParentsFactory):
                article = factory.SubFactory(ArticleFactory)
                customer_category = factory.SubFactory(CustomerCategoryFactory)
                distribution_channel = factory.SubFactory(DistributionChannelFactory)

            class ArticleCustomerCountry(RateActionNoParentsFactory):
                article = factory.SubFactory(ArticleFactory)
                customer = factory.SubFactory(CustomerFactory)
                country = factory.SubFactory(CountryFactory)

            class A2CustomerCountry(RateActionNoParentsFactory):
                article_range = factory.SubFactory(ArticleRangeFactory)
                commercialization_method = factory.SubFactory(
                    CommercializationMethodFactory
                )
                customer = factory.SubFactory(CustomerFactory)
                country = factory.SubFactory(CountryFactory)

            class ArticleC2Country(RateActionNoParentsFactory):
                article = factory.SubFactory(ArticleFactory)
                customer_category = factory.SubFactory(CustomerCategoryFactory)
                distribution_channel = factory.SubFactory(DistributionChannelFactory)
                country = factory.SubFactory(CountryFactory)

            class C2Country(RateActionNoParentsFactory):
                customer_category = factory.SubFactory(CustomerCategoryFactory)
                distribution_channel = factory.SubFactory(DistributionChannelFactory)
                country = factory.SubFactory(CountryFactory)

            class A2Country(RateActionNoParentsFactory):
                article_range = factory.SubFactory(ArticleRangeFactory)
                commercialization_method = factory.SubFactory(
                    CommercializationMethodFactory
                )
                country = factory.SubFactory(CountryFactory)

            class OnlyCountry(RateActionNoParentsFactory):
                country = factory.SubFactory(CountryFactory)

from src.core.models.customer_categories.CustomerCategory import CustomerCategory
from .factories import CustomerCategoryFactory


def test_create_customer_category(session):
    quantity = 5
    CustomerCategoryFactory.create_batch(quantity)
    assert session.query(CustomerCategory).count() == quantity

import factory

from src.core.models.customer_categories.CustomerCategory import CustomerCategory
from tests import config


class CustomerCategoryFactory(factory.alchemy.SQLAlchemyModelFactory):
    id = factory.Sequence(lambda n: "%s" % n)
    name = factory.Faker("word")

    class Meta:
        model = CustomerCategory
        sqlalchemy_session = config.Session

import factory

from src.core.models.article_ranges.ArticleRange import ArticleRange
from tests import config


class ArticleRangeFactory(factory.alchemy.SQLAlchemyModelFactory):
    id = factory.Sequence(lambda n: "%s" % n)
    name = factory.Faker("word")

    class Meta:
        model = ArticleRange
        sqlalchemy_session = config.Session
        sqlalchemy_get_or_create = ("name",)

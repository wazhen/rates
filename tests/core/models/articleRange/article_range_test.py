from src.core.models.article_ranges.ArticleRange import ArticleRange
from .factories import ArticleRangeFactory


def test_create_article_range(session):
    quantity = 5
    ArticleRangeFactory.create_batch(quantity)
    assert session.query(ArticleRange).count() == quantity

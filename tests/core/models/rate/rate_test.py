import pytest
from sqlalchemy.exc import IntegrityError

from src.core.models.rates.Rate import Rate
from .factories import RateFactory


class TestRate:
    class TestDunders:
        def test_eq(self, session):
            rate = RateFactory()
            session.query(Rate).all()
            assert not rate == 1
            assert not rate == "Abc"
            assert not rate == [1, "Abc"]

        def test_str(self, session):
            rate = RateFactory()
            session.query(Rate).all()
            str(rate)

    def test_create_rate(self, session):
        quantity = 5
        RateFactory.create_batch(quantity)
        assert session.query(Rate).count() == quantity

    def test_unique_constraint(self, session):
        with pytest.raises(IntegrityError):
            rate_1 = RateFactory.create()
            RateFactory.create(
                customer_category=rate_1.customer_category,
                distribution_channel=rate_1.distribution_channel,
                article_range=rate_1.article_range,
                commercialization_method=rate_1.commercialization_method,
                country=rate_1.country,
            )
            session.query(Rate).all()

import random

import factory

from src.core.models.rates.Rate import Rate
from ..articleRange.factories import ArticleRangeFactory
from ..commercializationMethod.factories import CommercializationMethodFactory
from ..country.factories import CountryFactory
from ..customerCategory.factories import CustomerCategoryFactory
from ..distributionChannel.factories import DistributionChannelFactory
from tests import config


class RateFactory(factory.alchemy.SQLAlchemyModelFactory):
    id = factory.Sequence(lambda n: "%s" % n)
    customer_category = factory.SubFactory(CustomerCategoryFactory)
    distribution_channel = factory.SubFactory(DistributionChannelFactory)
    article_range = factory.SubFactory(ArticleRangeFactory)
    commercialization_method = factory.SubFactory(CommercializationMethodFactory)
    country = factory.SubFactory(CountryFactory)
    discount_percentage = factory.lazy_attribute(lambda obj: random.random())
    review_date = factory.Faker("date_object")

    class Meta:
        model = Rate
        sqlalchemy_session = config.Session

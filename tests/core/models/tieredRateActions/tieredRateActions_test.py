from tests.core.models.tieredRateActions.factories import TieredRateActionsFactory
from .factories import TieredRateAction


def test_create_commercialization_method(session):
    quantity = 5
    TieredRateActionsFactory.create_batch(quantity)
    assert session.query(TieredRateAction).count() == quantity

import datetime
import random

import factory

from src.core.models.tiered_rate_actions.TieredRateAction import TieredRateAction
from ..article.factories import ArticleFactory
from ..articleRange.factories import ArticleRangeFactory
from ..commercializationMethod.factories import CommercializationMethodFactory
from ..country.factories import CountryFactory
from ..customer.factories import CustomerFactory
from ..customerCategory.factories import CustomerCategoryFactory
from ..distributionChannel.factories import DistributionChannelFactory
from tests import config


class TieredRateActionsFactory(factory.alchemy.SQLAlchemyModelFactory):
    id = factory.Sequence(lambda n: "%s" % n)
    customer_category = factory.SubFactory(CustomerCategoryFactory)
    distribution_channel = factory.SubFactory(DistributionChannelFactory)
    article_range = factory.SubFactory(ArticleRangeFactory)
    commercialization_method = factory.SubFactory(CommercializationMethodFactory)
    country = factory.SubFactory(CountryFactory)
    customer = factory.SubFactory(CustomerFactory)
    article = factory.SubFactory(ArticleFactory)
    start_date = factory.Faker("date_object")
    end_date = factory.lazy_attribute(
        lambda obj: obj.start_date + datetime.timedelta(days=random.randint(1, 180))
    )
    discount_percentage_1 = factory.lazy_attribute(lambda obj: random.randrange(0, 30))
    threshold_1 = factory.lazy_attribute(lambda obj: random.randrange(0, 10000))
    discount_percentage_2 = factory.lazy_attribute(lambda obj: random.randrange(30, 40))
    threshold_2 = factory.lazy_attribute(lambda obj: random.randrange(10000, 20000))
    discount_percentage_3 = factory.lazy_attribute(lambda obj: random.randrange(40, 50))

    class Meta:
        model = TieredRateAction
        sqlalchemy_session = config.Session

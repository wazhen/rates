from src.core.models.customers.Customer import Customer
from .factories import CustomerFactory


class TestCustomer:
    class TestDunders:
        def test_eq(self, session):
            customer = CustomerFactory.create()
            session.query(Customer).all()
            assert not customer == 1
            assert not customer == "Abc"
            assert not customer == [1, "Abc"]

    def test_create_customer(self, session):
        quantity = 5
        CustomerFactory.create_batch(quantity)
        assert session.query(Customer).count() == quantity

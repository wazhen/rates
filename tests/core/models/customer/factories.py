import factory

from src.core.models.customers.Customer import Customer
from tests.core.models.customerCategory.factories import CustomerCategoryFactory
from tests.core.models.distributionChannel.factories import DistributionChannelFactory
from tests import config


class CustomerFactory(factory.alchemy.SQLAlchemyModelFactory):
    id = factory.Sequence(lambda n: "%s" % n)
    name = factory.Faker("name")
    cif = factory.Faker("isbn13")
    customer_category = factory.SubFactory(CustomerCategoryFactory)
    distribution_channel = factory.SubFactory(DistributionChannelFactory)

    class Meta:
        model = Customer
        sqlalchemy_session = config.Session
        sqlalchemy_get_or_create = ("cif",)

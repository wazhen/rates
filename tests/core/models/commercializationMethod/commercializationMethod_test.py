from tests.core.models.commercializationMethod.factories import (
    CommercializationMethodFactory,
)
from .factories import CommercializationMethod


def test_create_commercialization_method(session):
    quantity = 5
    CommercializationMethodFactory.create_batch(quantity)
    assert session.query(CommercializationMethod).count() == quantity

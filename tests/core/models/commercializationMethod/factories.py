import factory

from src.core.models.commercialization_methods.CommercializationMethod import (
    CommercializationMethod,
)
from tests import config


class CommercializationMethodFactory(factory.alchemy.SQLAlchemyModelFactory):
    id = factory.Sequence(lambda n: "%s" % n)
    name = factory.Faker("word")

    class Meta:
        model = CommercializationMethod
        sqlalchemy_session = config.Session
        sqlalchemy_get_or_create = ("name",)

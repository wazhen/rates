from src.core.models.articles.Article import Article
from tests.core.models.article.factories import ArticleFactory


class TestArticle:
    class TestDunders:
        def test_eq(self, session):
            article = ArticleFactory.create()
            session.query(Article).all()
            assert not article == 1
            assert not article == "Abc"
            assert not article == [1, "abc"]

    def test_create_article(self, session):
        quantity = 5
        ArticleFactory.create_batch(quantity)
        assert session.query(Article).count() == quantity

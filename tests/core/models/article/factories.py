import factory

from src.core.models.articles.Article import Article
from tests import config
from ..articleRange.factories import ArticleRangeFactory
from ..commercializationMethod.factories import CommercializationMethodFactory


class ArticleFactory(factory.alchemy.SQLAlchemyModelFactory):
    id = factory.Sequence(lambda n: "%s" % n)
    name = factory.Faker("word")
    sku = factory.Faker("ean")
    commercialization_method = factory.SubFactory(CommercializationMethodFactory)
    article_range = factory.SubFactory(ArticleRangeFactory)

    class Meta:
        model = Article
        sqlalchemy_session = config.Session

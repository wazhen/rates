from src.core.models.articles import crud
from tests.core.models.article.factories import ArticleFactory, Article


class TestArticleCrud:
    def test_get_article(self, session):
        article = ArticleFactory.create()
        session.flush()
        query_article = crud.ArticlesCrud.get_by_id(session, article.id)
        assert query_article == article
        query_no_exists = crud.ArticlesCrud.get_by_id(session, int(article.id) + 1)
        assert query_no_exists != article

    def test_get_article_by_sku(self, session):
        article = ArticleFactory.create()
        session.flush()
        query_article = crud.ArticlesCrud.get_by_sku(session, article.sku)
        assert article == query_article
        query_no_exists = crud.ArticlesCrud.get_by_sku(session, "AAAAAAA")
        assert query_no_exists != article

    def test_get_articles(self, session):
        ArticleFactory.create_batch(150)
        session.flush()
        query_list = crud.ArticlesCrud.get_list(session, skip=0, limit=100)
        assert len(query_list) == 100
        query_list = crud.ArticlesCrud.get_list(session, skip=100, limit=100)
        assert len(query_list) == 50

    def test_create_article(self, session):
        article_for_use_masters = ArticleFactory.create()
        session.flush()
        article = Article(
            name="Conga 990",
            sku="00123",
            commercialization_method_id=article_for_use_masters.commercialization_method_id,
            article_range_id=article_for_use_masters.article_range_id,
        )
        articles_before = session.query(Article).count()
        crud.ArticlesCrud.create(session, article)
        articles_after = session.query(Article).count()
        assert (articles_before + 1) == articles_after

from tests.core.models.tieredRate.factories import TieredRateFactory
from .factories import TieredRate


def test_create_commercialization_method(session):
    quantity = 5
    TieredRateFactory.create_batch(quantity)
    assert session.query(TieredRate).count() == quantity

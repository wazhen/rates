import factory
import pycountry
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from src.main import app
from tests.core.models.country.factories import CountryFactory, Country

client = TestClient(app)


class TestCountryEndpoints:
    base_url = "/api/v1/countries/"

    def test_read_countries(self, session: Session):
        CountryFactory.create_batch(200)
        session.commit()
        qty_countries = session.query(Country).count()
        response = client.get(f"{self.base_url}?skip=0&limit=100")
        assert response.status_code == 200
        assert len(response.json()) == 100
        if qty_countries > 100:
            response = client.get("/api/v1/countries/?skip=100&limit=100")
            assert response.status_code == 200
            assert len(response.json()) == qty_countries - 100

    def test_create_country(self, session: Session):
        country_iso_2 = factory.Faker("country_code").generate()
        country_name = pycountry.countries.get(alpha_2=country_iso_2).name
        response = client.post(
            self.base_url,
            json={
                "name": country_name,
                "iso_2": country_iso_2
            }
        )
        assert session.query(Country).count() == 1
        assert response.status_code == 201
        assert (
            f'"name":"{country_name}","iso_2":"{country_iso_2}","id":'
            in response.text
        )

    def test_400_iso_2_already_registered(self, session: Session):
        country = CountryFactory.create()
        session.commit()
        response = client.post(
            self.base_url,
            json={
                "name": factory.Faker("word").generate(),
                "iso_2": country.iso_2
            }
        )
        assert response.status_code == 400

    def test_400_name_already_registered(self, session: Session):
        country = CountryFactory.create()
        session.commit()
        response = client.post(
            self.base_url,
            json={
                "name": country.name,
                "iso_2": factory.Faker("country_code").generate()
            }
        )
        assert response.status_code == 400

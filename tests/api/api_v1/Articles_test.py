import factory
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from src.main import app
from tests.core.models.article.factories import ArticleFactory, Article

client = TestClient(app)


class TestArticlesEndpoints:
    base_url = "/api/v1/articles/"

    def test_read(self, session: Session):
        ArticleFactory.create_batch(15)
        session.commit()
        response = client.get(f"{self.base_url}")
        assert response.status_code == 200
        assert len(response.json()) == 15

    def test_create(self, session: Session):
        article_for_masters = ArticleFactory.create()
        session.flush()
        assert session.query(Article).count() == 1
        art_name = factory.Faker("word").generate()
        art_sku = factory.Faker("ean").generate()
        response = client.post(
            f"{self.base_url}",
            json={
                "name": art_name,
                "sku": art_sku,
                "commercialization_method_id": article_for_masters.commercialization_method.id,
                "article_range_id": article_for_masters.article_range.id,
            },
        )
        assert session.query(Article).count() == 2
        assert response.status_code == 201
        assert (
            (
                '"name":"{}","sku":"{}","commercialization_method_id":'
                + '{},"article_range_id":{},"id":'
            ).format(
                art_name,
                art_sku,
                article_for_masters.commercialization_method.id,
                article_for_masters.article_range.id,
            )
            in response.text
        )

    def test_400_sku_already_registered(self, session: Session):
        article = ArticleFactory.create()
        session.commit()
        response = client.post(
            f"{self.base_url}",
            json={
                "name": factory.Faker("word").generate(),
                "sku": article.sku,
                "commercialization_method_id": article.commercialization_method_id,
                "article_range_id": article.article_range_id
            }
        )
        assert response.status_code == 400

import factory
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from src.main import app
from tests.core.models.customer.factories import CustomerFactory, Customer

client = TestClient(app)


class TestCustomersEndpoints:
    base_url = "/api/v1/customers/"

    def test_read(self, session: Session):
        CustomerFactory.create_batch(15)
        session.commit()
        response = client.get(f"{self.base_url}")
        assert response.status_code == 200
        assert len(response.json()) == 15

    def test_create(self, session: Session):
        customer_for_masters = CustomerFactory.create()
        session.flush()
        assert session.query(Customer).count() == 1
        clt_name = factory.Faker("word").generate()
        clt_cif = factory.Faker("ean").generate()
        response = client.post(
            f"{self.base_url}",
            json={
                "name": clt_name,
                "cif": clt_cif,
                "customer_category_id": customer_for_masters.customer_category.id,
                "distribution_channel_id": customer_for_masters.distribution_channel.id,
            },
        )
        assert session.query(Customer).count() == 2
        assert response.status_code == 201
        assert (
            (
                '"name":"{}","cif":"{}","customer_category_id":'
                + '{},"distribution_channel_id":{},"id":'
            ).format(
                clt_name,
                clt_cif,
                customer_for_masters.customer_category.id,
                customer_for_masters.distribution_channel.id,
            )
            in response.text
        )

    def test_400_cif_already_registered(self, session: Session):
        customer = CustomerFactory.create()
        session.commit()
        response = client.post(
            f"{self.base_url}",
            json={
                "name": factory.Faker("word").generate(),
                "cif": customer.cif,
                "customer_category_id": customer.customer_category_id,
                "distribution_channel_id": customer.distribution_channel_id
            }
        )
        assert response.status_code == 400

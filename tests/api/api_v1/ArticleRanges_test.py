import factory
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from src.main import app
from tests.core.models.articleRange.factories import ArticleRange, ArticleRangeFactory

client = TestClient(app)


class TestArticleRangeEndpoints:
    def test_create_article_range(self, session: Session):
        ar_name = factory.Faker("word").generate()
        response = client.post(
            "/api/v1/article_ranges/",
            json={
                "name": ar_name
            }
        )
        assert session.query(ArticleRange).count() == 1
        assert response.status_code == 201
        assert '"name":"{}","id":'.format(ar_name) in response.text

    def test_read_article_ranges(self, session: Session):
        ArticleRangeFactory.create_batch(150)
        session.commit()
        quantity = session.query(ArticleRange).count()
        response = client.get("/api/v1/article_ranges/")
        assert response.status_code == 200
        assert len(response.json()) == 100
        if quantity > 100:
            response = client.get("api/v1/article_ranges/?skip=100&limit=100")
            assert response.status_code == 200
            assert len(response.json()) == quantity - 100

    def test_400_name_already_registered(self, session: Session):
        created_article_range = ArticleRangeFactory.create()
        session.commit()
        response = client.post(
            "/api/v1/article_ranges/",
            json={
                "name": created_article_range.name
            }
        )
        assert response.status_code == 400

import factory
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from src.main import app
from tests.core.models.customerCategory.factories import (
    CustomerCategoryFactory, CustomerCategory
)

client = TestClient(app)


class TestCustomerCategoriesEndpoints:
    base_url = "/api/v1/customer_categories/"

    def test_read_customer_categories(self, session: Session):
        CustomerCategoryFactory.create_batch(150)
        session.commit()
        qty_cc = session.query(CustomerCategory).count()
        response = client.get(f"{self.base_url}?skip=0&limit=100")
        assert response.status_code == 200
        assert len(response.json()) == 100
        if qty_cc > 100:
            response = client.get(f"{self.base_url}?skip=100&limit=100")
            assert response.status_code == 200
            assert len(response.json()) == qty_cc - 100

    def test_create_customer_category(self, session: Session):
        customer_category_name = factory.Faker("word").generate()
        response = client.post(
            self.base_url,
            json={
                "name": customer_category_name
            }
        )
        assert session.query(CustomerCategory).count() == 1
        assert response.status_code == 201

    def test_400_name_already_registered(self, session: Session):
        customer_category = CustomerCategoryFactory.create()
        session.commit()
        response = client.post(
            self.base_url,
            json={
                "name": customer_category.name
            }
        )
        assert response.status_code == 400

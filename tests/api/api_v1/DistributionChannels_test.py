import factory
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from src.main import app
from tests.core.models.distributionChannel.factories import (
    DistributionChannelFactory, DistributionChannel
)

client = TestClient(app)


class TestCustomerCategoriesEndpoints:
    base_url = "/api/v1/distribution_channels/"

    def test_read(self, session: Session):
        DistributionChannelFactory.create_batch(150)
        session.commit()
        qty = session.query(DistributionChannel).count()
        response = client.get(f"{self.base_url}?skip=0&limit=100")
        assert response.status_code == 200
        assert len(response.json()) == 100
        if qty > 100:
            response = client.get(f"{self.base_url}?skip=100&limit=100")
            assert response.status_code == 200
            assert len(response.json()) == qty - 100

    def test_create(self, session: Session):
        name = factory.Faker("word").generate()
        response = client.post(
            self.base_url,
            json={
                "name": name
            }
        )
        assert session.query(DistributionChannel).count() == 1
        assert response.status_code == 201

    def test_400_name_already_registered(self, session: Session):
        register = DistributionChannelFactory.create()
        session.commit()
        response = client.post(
            self.base_url,
            json={
                "name": register.name
            }
        )
        assert response.status_code == 400

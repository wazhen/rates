import factory
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from src.main import app
from tests.core.models.commercializationMethod.factories import (
    CommercializationMethod,
    CommercializationMethodFactory
)

client = TestClient(app)


class TestCommercializationMethodsEndpoints:
    def test_create_commercialization_method(self, session: Session):
        cm_name = factory.Faker("word").generate()
        response = client.post(
            "/api/v1/commercialization_methods/",
            json={
                "name": cm_name
            }
        )
        assert session.query(CommercializationMethod).count() == 1
        assert response.status_code == 201
        assert '"name":"{}","id":'.format(cm_name) in response.text

    def test_read_commercialization_methods(self, session: Session):
        CommercializationMethodFactory.create_batch(150)
        session.commit()
        quantity = session.query(CommercializationMethod).count()
        response = client.get("/api/v1/commercialization_methods/")
        assert response.status_code == 200
        assert len(response.json()) == 100
        if quantity > 100:
            response = client.get(
                "/api/v1/commercialization_methods/?skip=100&limit=100"
            )
            assert response.status_code == 200
            assert len(response.json()) == quantity - 100

    def test_400_name_already_registered(self, session: Session):
        cm = CommercializationMethodFactory.create()
        session.commit()
        response = client.post(
            "/api/v1/commercialization_methods/",
            json={
                "name": cm.name
            }
        )
        assert response.status_code == 400

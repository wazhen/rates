from sqlalchemy import orm

import src.core.config

Session = orm.scoped_session(orm.sessionmaker())
Session.configure(bind=src.core.config.engine)

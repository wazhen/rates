from fastapi.testclient import TestClient
from src.main import app

client = TestClient(app)


class TestMain:
    def test_ping(self):
        response = client.get("/ping")
        assert response.status_code == 200
        assert response.text == '{"ping":"pong!"}'

from unittest import mock

import pytest

import src.core.config
from src.core import config
from src.main import app
from tests.config import Session


@pytest.fixture
def session():
    session = Session()
    app.dependency_overrides[src.core.config.get_db] = lambda: session
    with mock.patch.object(config, "session", session):
        session.begin_nested()
        yield session
        session.rollback()
        Session.remove()
        app.dependency_overrides = {}

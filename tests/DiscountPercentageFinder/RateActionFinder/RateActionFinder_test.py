from src.DiscountPercentageFinder.RateActionFinder.RateActionFinder import (
    RateActionFinder,
)
from tests.core.models.article.factories import ArticleFactory
from tests.core.models.articleRange.factories import ArticleRangeFactory
from tests.core.models.commercializationMethod.factories import (
    CommercializationMethodFactory,
)
from tests.core.models.country.factories import CountryFactory
from tests.core.models.customer.factories import CustomerFactory
from tests.core.models.customerCategory.factories import CustomerCategoryFactory
from tests.core.models.distributionChannel.factories import DistributionChannelFactory
from tests.core.models.rateAction.factories import RateActionFactory


class TestRateActionFinder:
    def set_up(self):
        self.country = CountryFactory.create()
        self.customer_category = CustomerCategoryFactory.create()
        self.distribution_channel = DistributionChannelFactory.create()
        self.article_range = ArticleRangeFactory.create()
        self.commercialization_method = CommercializationMethodFactory.create()
        self.customer = CustomerFactory.create()
        self.article = ArticleFactory.create()

    def rate_action_c2_a2_country(self, session):
        return RateActionFinder(
            session,
            customer_category=self.customer_category,
            distribution_channel=self.distribution_channel,
            article_range=self.article_range,
            commercialization_method=self.commercialization_method,
            country_iso_2=self.country.iso_2,
        )

    def rate_action_article_customer_country(self, session):
        return RateActionFinder(
            session,
            customer_cif=self.customer.cif,
            article_sku=self.article.sku,
            country_iso_2=self.country.iso_2,
        )

    def test_get_country(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_c2_a2_country(session)
        assert rate_action_finder.country == self.country

    def test_get_customer_get_article(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_article_customer_country(session)
        assert rate_action_finder.customer == self.customer
        assert rate_action_finder.article == self.article

    def test_get_a2_c2_country_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_article_customer_country(session)
        rate_action = RateActionFactory.Valid.Mixed.A2C2Country.create(
            customer_category=rate_action_finder.customer.customer_category,
            distribution_channel=rate_action_finder.customer.distribution_channel,
            article_range=rate_action_finder.article.article_range,
            commercialization_method=rate_action_finder.article.commercialization_method,
            country=rate_action_finder.country,
        )
        rate_action_list = rate_action_finder.get_a2_c2_country_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_article_customer_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_article_customer_country(session)
        rate_action = RateActionFactory.Valid.Mixed.ArticleCustomer.create(
            article=rate_action_finder.article, customer=rate_action_finder.customer
        )
        rate_action_list = rate_action_finder.get_article_customer_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_customer_country_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_article_customer_country(session)
        rate_action = RateActionFactory.Valid.Mixed.CustomerCountry.create(
            customer=rate_action_finder.customer, country=rate_action_finder.country
        )
        rate_action_list = rate_action_finder.get_customer_country_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_article_country_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_article_customer_country(session)
        rate_action = RateActionFactory.Valid.Mixed.ArticleCountry.create(
            article=rate_action_finder.article, country=rate_action_finder.country
        )
        rate_action_list = rate_action_finder.get_article_country_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_a2_customer_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_article_customer_country(session)
        rate_action = RateActionFactory.Valid.Mixed.A2Customer.create(
            customer=rate_action_finder.customer,
            article_range=rate_action_finder.article_range,
            commercialization_method=rate_action_finder.commercialization_method,
        )
        rate_action_list = rate_action_finder.get_a2_customer_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_article_c2_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_article_customer_country(session)
        rate_action = RateActionFactory.Valid.Mixed.ArticleC2.create(
            article=rate_action_finder.article,
            customer_category=rate_action_finder.customer_category,
            distribution_channel=rate_action_finder.distribution_channel,
        )
        rate_action_list = rate_action_finder.get_article_c2_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_article_customer_country_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_article_customer_country(session)
        rate_action = RateActionFactory.Valid.Mixed.ArticleCustomerCountry.create(
            article=rate_action_finder.article,
            customer=rate_action_finder.customer,
            country=rate_action_finder.country,
        )
        rate_action_list = rate_action_finder.get_article_customer_country_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_a2_customer_country_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_article_customer_country(session)
        rate_action = RateActionFactory.Valid.Mixed.A2CustomerCountry.create(
            article_range=rate_action_finder.article_range,
            commercialization_method=rate_action_finder.commercialization_method,
            customer=rate_action_finder.customer,
            country=rate_action_finder.country,
        )
        rate_action_list = rate_action_finder.get_a2_customer_country_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_article_c2_country_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_article_customer_country(session)
        rate_action = RateActionFactory.Valid.Mixed.ArticleC2Country.create(
            article=rate_action_finder.article,
            customer_category=rate_action_finder.customer_category,
            distribution_channel=rate_action_finder.distribution_channel,
            country=rate_action_finder.country,
        )
        rate_action_list = rate_action_finder.get_article_c2_country_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_customer_rate_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_article_customer_country(session)
        rate_action = RateActionFactory.Valid.CustomerRelated.Customer.create(
            customer=rate_action_finder.customer
        )
        rate_action_list = rate_action_finder.get_customer_rate_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_article_rate_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_article_customer_country(session)
        rate_action = RateActionFactory.Valid.ArticleRelated.Article.create(
            article=rate_action_finder.article
        )
        rate_action_list = rate_action_finder.get_article_rate_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_c2_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_c2_a2_country(session)
        rate_action = RateActionFactory.Valid.CustomerRelated.C2.create(
            customer_category=rate_action_finder.customer_category,
            distribution_channel=rate_action_finder.distribution_channel,
        )
        rate_action_list = rate_action_finder.get_c2_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_c2_country_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_c2_a2_country(session)
        rate_action = RateActionFactory.Valid.Mixed.C2Country.create(
            customer_category=rate_action_finder.customer_category,
            distribution_channel=rate_action_finder.distribution_channel,
            country=rate_action_finder.country,
        )
        rate_action_list = rate_action_finder.get_c2_country_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_a2_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_c2_a2_country(session)
        rate_action = RateActionFactory.Valid.ArticleRelated.A2.create(
            article_range=rate_action_finder.article_range,
            commercialization_method=rate_action_finder.commercialization_method,
        )
        rate_action_list = rate_action_finder.get_a2_rate_list()
        assert rate_action_list[0] == rate_action

    def test_get_a2_country_rate_list(self, session):
        self.set_up()
        rate_action_finder = self.rate_action_c2_a2_country(session)
        rate_action = RateActionFactory.Valid.Mixed.A2Country.create(
            article_range=rate_action_finder.article_range,
            commercialization_method=rate_action_finder.commercialization_method,
            country=rate_action_finder.country,
        )
        rate_action_list = rate_action_finder.get_a2_country_rate_list()
        assert rate_action_list[0] == rate_action

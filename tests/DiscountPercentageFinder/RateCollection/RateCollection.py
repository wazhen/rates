from tests.core.models.rate.factories import RateFactory, Rate
from tests.core.models.rateAction.factories import RateActionFactory, RateAction


class RateCollection:
    class Rates:
        pass

    def __init__(self):
        self.Rates.standard = RateFactory.create()
        self.Rates.article_customer = (
            RateActionFactory.Valid.Mixed.ArticleCustomer.create()
        )
        self.customer_category = self.Rates.standard.customer_category
        self.distribution_channel = self.Rates.standard.distribution_channel
        self.article_range = self.Rates.standard.article_range
        self.commercialization_method = self.Rates.standard.commercialization_method
        self.country = self.Rates.standard.country
        self.article = self.Rates.article_customer.article
        self.customer = self.Rates.article_customer.customer
        self.Rates.c2_a2_country = RateActionFactory.Valid.Mixed.A2C2Country.create(
            customer_category=self.customer_category,
            distribution_channel=self.distribution_channel,
            article_range=self.article_range,
            commercialization_method=self.commercialization_method,
        )
        self.Rates.customer_country = RateActionFactory.Valid.Mixed.CustomerCountry.create(
            customer=self.customer, country=self.country,
        )
        self.Rates.article_country = RateActionFactory.Valid.Mixed.ArticleCountry.create(
            article=self.article, country=self.country,
        )
        self.Rates.a2_customer = RateActionFactory.Valid.Mixed.A2Customer.create(
            article_range=self.article_range,
            commercialization_method=self.commercialization_method,
            customer=self.customer,
        )
        self.Rates.article_c2 = RateActionFactory.Valid.Mixed.ArticleC2.create(
            article=self.article,
            customer_category=self.customer_category,
            distribution_channel=self.distribution_channel,
        )
        self.Rates.article_customer_country = RateActionFactory.Valid.Mixed.ArticleCustomerCountry.create(
            article=self.article, customer=self.customer, country=self.country,
        )
        self.Rates.a2_customer_country = RateActionFactory.Valid.Mixed.A2CustomerCountry.create(
            article_range=self.article_range,
            commercialization_method=self.commercialization_method,
            customer=self.customer,
            country=self.country,
        )
        self.Rates.article_c2_country = RateActionFactory.Valid.Mixed.ArticleC2Country.create(
            article=self.article,
            customer_category=self.customer_category,
            distribution_channel=self.distribution_channel,
            country=self.country,
        )
        self.Rates.customer = RateActionFactory.Valid.CustomerRelated.Customer.create(
            customer=self.customer,
        )
        self.Rates.article = RateActionFactory.Valid.ArticleRelated.Article.create(
            article=self.article,
        )
        self.Rates.c2 = RateActionFactory.Valid.CustomerRelated.C2.create(
            customer_category=self.customer_category,
            distribution_channel=self.distribution_channel,
        )
        self.Rates.c2_country = RateActionFactory.Valid.Mixed.C2Country.create(
            customer_category=self.customer_category,
            distribution_channel=self.distribution_channel,
            country=self.country,
        )
        self.Rates.a2 = RateActionFactory.Valid.ArticleRelated.A2.create(
            article_range=self.article_range,
            commercialization_method=self.commercialization_method,
        )
        self.Rates.a2_country = RateActionFactory.Valid.Mixed.A2Country.create(
            article_range=self.article_range,
            commercialization_method=self.commercialization_method,
            country=self.country,
        )
        self.Rates.country = RateActionFactory.Valid.Mixed.OnlyCountry.create(
            country=self.country,
        )
        self.rate_list = [
            self.Rates.standard,
            self.Rates.c2_a2_country,
            self.Rates.article_customer,
            self.Rates.customer_country,
            self.Rates.article_country,
            self.Rates.a2_customer,
            self.Rates.article_c2,
            self.Rates.article_customer_country,
            self.Rates.a2_customer_country,
            self.Rates.article_c2_country,
            self.Rates.customer,
            self.Rates.article,
            self.Rates.c2,
            self.Rates.c2_country,
            self.Rates.a2,
            self.Rates.a2_country,
            self.Rates.country,
        ]

    def __iter__(self):
        return iter(self.rate_list)

    def __len__(self):
        return len(self.rate_list)

    def min_rate(self):
        return sorted(
            self.rate_list, key=lambda i: i.discount_percentage, reverse=False
        )[0]

    def max_rate(self):
        return sorted(
            self.rate_list, key=lambda x: x.discount_percentage, reverse=True
        )[0]

    def make_best(self, rate):
        if isinstance(rate, Rate) or isinstance(rate, RateAction):
            if rate in self.rate_list:
                min_rate = self.min_rate()

                if min_rate.discount_percentage <= rate.discount_percentage:
                    rate.discount_percentage = min_rate.discount_percentage
                    min_rate.discount_percentage += 0.1
            else:
                raise TypeError("rate should be in RateCollection.rate_list")
        else:
            raise TypeError("Make_best accepts Rate or RateAction as parameter")

import pytest

from src.DiscountPercentageFinder.DiscountPercentageFinder import (
    DiscountPercentageFinder,
)
from tests.core.models.articleRange.factories import ArticleRangeFactory
from tests.core.models.customerCategory.factories import CustomerCategoryFactory
from .RateCollection import RateCollection


class TestDiscountPercentageFinder:
    class TestExpectValueError:
        def test_no_parameters(self, session):
            with pytest.raises(ValueError):
                DiscountPercentageFinder(session)

        def test_article_a2(self, session):
            with pytest.raises(AssertionError):
                article_range = ArticleRangeFactory.create()
                DiscountPercentageFinder(
                    session, article_sku="123", article_range=article_range
                )

        def test_customer_c2(self, session):
            with pytest.raises(AssertionError):
                customer_category = CustomerCategoryFactory.create()
                DiscountPercentageFinder(
                    session, customer_cif="abc", customer_category=customer_category
                )

        class TestIncorrectTypes:
            def test_customer_category(self, session):
                with pytest.raises(ValueError):
                    DiscountPercentageFinder(session, customer_category=1)

            def test_distribution_channel(self, session):
                with pytest.raises(ValueError):
                    DiscountPercentageFinder(session, distribution_channel=1)

            def test_article_range(self, session):
                with pytest.raises(ValueError):
                    DiscountPercentageFinder(session, article_range=1)

            def test_commercialization_method(self, session):
                with pytest.raises(ValueError):
                    DiscountPercentageFinder(session, commercialization_method=1)

    def test_rate_finder(self, session):
        rate_collection = RateCollection()
        rate_collection.make_best(rate_collection.Rates.standard)
        discount_percentage_finder = DiscountPercentageFinder(
            session,
            customer_category=rate_collection.Rates.standard.customer_category,
            distribution_channel=rate_collection.Rates.standard.distribution_channel,
            article_range=rate_collection.Rates.standard.article_range,
            commercialization_method=rate_collection.Rates.standard.commercialization_method,
            country_iso_2=rate_collection.Rates.standard.country.iso_2,
        )
        rate = discount_percentage_finder.get_rate()
        assert rate == rate_collection.Rates.standard

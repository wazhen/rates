import pytest

from src.DiscountPercentageFinder.RateFinder.RateFinder import RateFinder
from tests.core.models.article.factories import ArticleFactory
from tests.core.models.customer.factories import CustomerFactory
from tests.core.models.rate.factories import RateFactory


class TestRateFinder:
    class TestCreationValidation:
        def test_no_kw_params(self, session):
            with pytest.raises(ValueError):
                RateFinder(session)

        def test_incorrect_session_type(self):
            with pytest.raises(AssertionError):
                RateFinder("string_where_Session_expected")

        def test_only_customer_category(self, session):
            with pytest.raises(AssertionError):
                rate = RateFactory()
                RateFinder(
                    session,
                    country_iso_2=rate.country.iso_2,
                    article_range=rate.article_range,
                    commercialization_method=rate.commercialization_method,
                    customer_category=rate.customer_category,
                )

        def test_only_distribution_channel(self, session):
            with pytest.raises(AssertionError):
                rate = RateFactory()
                RateFinder(
                    session,
                    country_iso_2=rate.country.iso_2,
                    article_range=rate.article_range,
                    commercialization_method=rate.commercialization_method,
                    distribution_channel=rate.distribution_channel,
                )

    def test_get_rate_by_a2_c2(self, session):
        rate = RateFactory.create()
        rate_finder = RateFinder(
            session,
            customer_category=rate.customer_category,
            distribution_channel=rate.distribution_channel,
            article_range=rate.article_range,
            commercialization_method=rate.commercialization_method,
            country_iso_2=rate.country.iso_2,
        )
        result = rate_finder.get_rate()
        assert result == rate

    def test_get_rate_by_a2_customer(self, session):
        rate = RateFactory.create()
        customer = CustomerFactory.create(
            customer_category=rate.customer_category,
            distribution_channel=rate.distribution_channel,
        )
        rate_finder = RateFinder(
            session,
            country_iso_2=rate.country.iso_2,
            customer_cif=customer.cif,
            article_range=rate.article_range,
            commercialization_method=rate.commercialization_method,
        )
        result = rate_finder.get_rate()
        assert result == rate

    def test_get_rate_by_article_c2(self, session):
        rate = RateFactory.create()
        article = ArticleFactory.create(
            article_range=rate.article_range,
            commercialization_method=rate.commercialization_method,
        )
        rate_finder = RateFinder(
            session,
            article_sku=article.sku,
            customer_category=rate.customer_category,
            distribution_channel=rate.distribution_channel,
            country_iso_2=rate.country.iso_2,
        )
        result = rate_finder.get_rate()
        assert result == rate

    def test_get_rate_by_a2_c2(self, session):
        rate = RateFactory.create()
        article = ArticleFactory.create(
            article_range=rate.article_range,
            commercialization_method=rate.commercialization_method,
        )
        customer = CustomerFactory.create(
            customer_category=rate.customer_category,
            distribution_channel=rate.distribution_channel,
        )

        rate_finder = RateFinder(
            session,
            article_sku=article.sku,
            customer_cif=customer.cif,
            country_iso_2=rate.country.iso_2,
        )
        result = rate_finder.get_rate()
        assert result == rate

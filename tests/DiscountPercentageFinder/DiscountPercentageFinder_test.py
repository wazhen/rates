from src.DiscountPercentageFinder.DiscountPercentageFinder import (
    DiscountPercentageFinder,
)
from tests.DiscountPercentageFinder.RateCollection.RateCollection import RateCollection


class TestDiscountPercentageFinder:
    def test_get_rate(self, session):
        rate_collection = RateCollection()
        discount_percentage_finder = DiscountPercentageFinder(
            session,
            customer_cif=rate_collection.customer.cif,
            article_sku=rate_collection.article.sku,
            country_iso_2=rate_collection.country.iso_2,
        )
        rate = discount_percentage_finder.get_rate()
        assert rate == rate_collection.min_rate()

# Project

This project is a test to deploy an api with multiple endpoints in a single
aws lambda with an aws api-gateway

It uses mangum and fast api to make routing the request through code possible

# Setup

Create .env file in "/".

Example needed content:
```
DATABASE_ENGINE=postgresql+psycopg2
DATABASE_NAME=tarifas
DATABASE_USER=tarifas
DATABASE_PASSWORD=tarifaspass
DATABASE_HOST=localhost
DATABASE_PORT=5432
```

This proyect uses pipenv as package manager but not for managing virtual 
environmets. 

Run the following commands:

```console
python3.8 -m venv venv
source venv/bin/activate
pip install pipenv
pipenv install --dev
```

Make alembic migrations:

```console
alembic upgrade head
```

# Run tests to check correct setup

```console
pytest
```

## Local server and docs

Run local server:
```console
uvicorn src.main:app --reload --env-file .env
```

To check the documentation go to the following link

http://localhost:8000/docs
